<?php
session_start();
$pdo = null;


function criaConexaoBancoDados()
{
    global $pdo;
    if($pdo instanceof PDO){
        return $pdo;
    }
    $config = require_once __DIR__ . '/config/global.php';
    $database = $config['database']['blogNews'];
    $pdo = new PDO(sprintf("mysql:dbname=%s;host=%s;charset=%s", $database['database'],$database['hostname'], $database['charset']),$database['username'], $database['password']);
    //criar exception
    return $pdo;
}

function geraHashSenha($senha){
    $hash = md5($senha);
    return $hash;

}

function redireciona($endereco){
    //header("location: $endereco");
    printf("<script>window.location='%s'; </script>", $endereco);
}

function alertaJavascript($messagem){
    printf("<script>alert('%s');</script>", $messagem);
}

function estaLogado(){
    if(isset($_SESSION['logado']) && $_SESSION['logado'] === true){
        return true;
    }
    return false;
}