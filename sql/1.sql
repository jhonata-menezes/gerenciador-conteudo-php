SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema blognews
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `blognews` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `blognews` ;

-- -----------------------------------------------------
-- Table `blognews`.`usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blognews`.`usuario` ;

CREATE TABLE IF NOT EXISTS `blognews`.`usuario` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `senha` VARCHAR(100) NOT NULL,
  `nascimento` DATETIME NOT NULL,
  `criado` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_usuario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blognews`.`post`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blognews`.`post` ;

CREATE TABLE IF NOT EXISTS `blognews`.`post` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(255) NULL,
  `title` VARCHAR(255) NULL,
  `logo` VARCHAR(255) NULL,
  `texto` TEXT NULL,
  `rodape` TEXT NULL,
  `criado` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
