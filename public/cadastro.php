<?php
require_once __DIR__ . '/../autoload.php';
require_once __DIR__ . '/cabecalho.php';
?>

<div class="jumbotron">
    <div class="container">
        <form class="form-horizontal" action="cadastroPost.php" method="post">
            <fieldset>

                <!-- Form Name -->
                <legend>Formulário de Cadastro</legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Nome</label>
                    <div class="col-md-6">
                        <input id="textinput" name="nome" type="text" placeholder="Nome" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">E-mail</label>
                    <div class="col-md-5">
                        <input name="email" type="email" placeholder="Email" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="passwordinput">Senha</label>
                    <div class="col-md-4">
                        <input id="passwordinput" name="senha" type="password" placeholder="Senha" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton"></label>
                    <div class="col-md-4">
                        <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">Enviar</button>
                    </div>
                </div>


            </fieldset>
        </form>

    </div>
</div>

<?php
require_once __DIR__ . '/rodape.php';
?>