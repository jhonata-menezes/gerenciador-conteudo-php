<?php
require_once __DIR__ . '/../autoload.php';
require_once __DIR__ . '/cabecalho.php';

$conexao = criaConexaoBancoDados();
if(isset($_GET['pesquisa']) && strlen($_GET['pesquisa']) >= 1){
    $where = '%' . $_GET['pesquisa'] . '%';
    $colecaoDados = $conexao->prepare("select * from post WHERE texto LIKE :pesquisa OR  titulo LIKE :pesquisa");
    $colecaoDados->bindParam(':pesquisa', $where);
    $colecaoDados->execute();

}else{
    $colecaoDados = $conexao->prepare('select * from post LIMIT 20');
    $colecaoDados->execute();
}

?>
<br/>
<br/>
<br/>
<div class="container">
    <?php if(count($colecaoDados->rowCount()) === 0) { ?>
        <div class="page-header">
            <h1>Infezlimente não há nenhum post</h1>
        </div>

    <?php } ?>
    <?php foreach ($colecaoDados->fetchAll() as $registro) { ?>
        <div class="page-header">
            <h1><img src="/<?=$registro['logo']?>" class="img-responsive" alt="Logo" height="100px" width="100px"><?=strtoupper($registro['titulo'])?> <?php if(estaLogado()){ ?>
                    <a href="/visualizar.php?id=<?=$registro['id']?>" role="button" class="btn btn-primary ">Visualizar Post</a>
                    <a href="/editar.php?id=<?=$registro['id']?>" role="button" class="btn btn-warning ">Editar Post</a>
                <a href="/apagar.php?id=<?=$registro['id']?>" role="button" class="btn btn-danger ">Apagar Post</a>
                <?php } ?>
                </h1>
        </div>
        <div class="row">
            <?=substr($registro['texto'], 0, 200)?>
        </div>

    <?php } ?>

</div>

<div>
    
</div>

<?php
require_once __DIR__ . '/rodape.php';

?>