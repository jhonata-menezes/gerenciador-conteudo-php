<?php
require_once __DIR__ . '/../autoload.php';

if(!estaLogado()){
    alertaJavascript('É necessario estar logado para criar post');
    redireciona('/');
}

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_FILES['logo'],$_POST['titulo'], $_POST['texto'], $_POST['title'], $_POST['rodape'], $_POST['id'])){
    $destino = 'imagem/logo_' . rand(1,999) . '.' . pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
    move_uploaded_file($_FILES['logo']['tmp_name'], $destino);
    $prepare = criaConexaoBancoDados()->prepare('UPDATE post SET titulo=:titulo, texto=:texto, logo=:logo, title=:title, rodape=:rodape WHERE id=:id');
    $prepare->bindParam(':titulo', $_POST['titulo']);
    $prepare->bindParam(':title', $_POST['title']);
    $prepare->bindParam(':texto', $_POST['texto']);
    $prepare->bindParam(':rodape', $_POST['rodape']);
    $prepare->bindParam(':logo', $destino);
    $prepare->bindParam(':id', $_POST['id']);
    if($prepare->execute()){
        alertaJavascript('Post atualizado com sucesso');
        redireciona('/');
    }else{
        $prepare->errorInfo();
        var_dump($prepare->errorInfo()); die;
        alertaJavascript('Nao foi possivel editar o post');
        redireciona('/');
    }
}else{
    alertaJavascript('parametros incorreto');
    redireciona('/');
}