<html xmlns="http://www.w3.org/1999/html">
<head>
    <title><?=$titulo?></title>
    <link rel="icon" type="image/png" href="/imagem/<?=$icone?>" />
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Blog News</a>
        </div>
        <?php if(isset($_SESSION['logado']) && $_SESSION['logado']){ ?>
    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <?php echo strtoupper($_SESSION['usuario']['nome']) ?> <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li class="divider"></li>
                <li><a href="/logout.php"><span class="glyphicon glyphicon-log-out"></span> Sair </a></li>
            </ul>
        </li>
    </ul>

<?php } else{ ?>
    <div id="navbar" class="navbar-collapse collapse">
        <form class="navbar-form navbar-right" action="/cadastro.php" method="post">
            <button class="btn btn-primary" onclick="window.location='/cadastro.php'";>Cadastro</button>
        </form>
        <form class="navbar-form navbar-right" action="/login.php" method="post">
            <div class="form-group">
                <input type="text" placeholder="Email" name="email" class="form-control">
            </div>
            <div class="form-group">
                <input type="password" placeholder="senha" name="senha" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Login</button>

        </form>
    </div><!--/.navbar-collapse -->
<?php } ?>
</div>
</nav>
