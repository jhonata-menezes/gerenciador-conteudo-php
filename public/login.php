<?php
require_once __DIR__ . '/../autoload.php';
if($_SERVER['REQUEST_METHOD'] === 'POST'){
    if(isset($_POST['email'], $_POST['senha'])){
        $prepare = criaConexaoBancoDados()->prepare('select * from usuario where email=:email and senha=:senha');
        $prepare->bindParam(':email', $_POST['email']);
        $prepare->bindParam(':senha', geraHashSenha($_POST['senha']));
        $result = $prepare->execute();
        if($prepare->rowCount() === 1){
            $_SESSION['logado'] = true;
            $_SESSION['usuario'] = $prepare->fetchAll()[0];
            alertaJavascript('login efetuado com sucesso');
            redireciona('/');
        }else{
            alertaJavascript('email ou senha incorretos');
            redireciona('/');
        }
    }
    else{
        alertaJavascript('email ou senha não digitado');
        redireciona('/');
    }
}
