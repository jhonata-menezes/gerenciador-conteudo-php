<?php
require_once __DIR__ . '/../autoload.php';
require_once __DIR__ . '/cabecalho.php';

if(!estaLogado()){
    alertaJavascript('É necessario estar logado para criar post');
    redireciona('/');
}
?>

<script>
    function loadImageFileAsURL()
    {
        var filesSelected = document.getElementById("inputFileToLoad").files;
        if (filesSelected.length > 0)
        {
            var fileToLoad = filesSelected[0];

            var fileReader = new FileReader();

            fileReader.onload = function(fileLoadedEvent)
            {
                var textAreaFileContents = document.getElementById
                (
                    "textAreaFileContents"
                );

                textAreaFileContents.innerHTML = fileLoadedEvent.target.result;
            };

            fileReader.readAsDataURL(fileToLoad);
        }
    }

</script>
<br/>
<br/>
<br/>
<div class="container">
    <form class="form-horizontal" action="/criarPost.php" method="post" enctype="multipart/form-data">
        <fieldset>

            <!-- Form Name -->
            <legend>Criação de Post</legend>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="textinput">Title</label>
                <div class="col-md-6">
                    <input id="textinput" name="title" type="text" placeholder="Title" class="form-control input-md" required="">

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="textinput">Titulo</label>
                <div class="col-md-6">
                    <input id="textinput" name="titulo" type="text" placeholder="Titulo" class="form-control input-md" required="">

                </div>
            </div>
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="textinput">Logo do Site</label>
                <div class="col-md-5">
                    <input name="logo" type="file" placeholder="Logo" class="form-control input-md" accept="image/*" required="">

                </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="texto">Texto (aceita HTML)</label>
                <div class="col-md-4">
                    <textarea class="form-control" id="texto" name="texto" rows="20" cols="1" ></textarea>
                </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="texto">Rodape (aceita HTML)</label>
                <div class="col-md-4">
                    <textarea class="form-control" id="texto" name="rodape" rows="20" cols="1" ></textarea>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>
                <div class="col-md-4">
                    <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">Enviar</button>
                </div>
            </div>

        </fieldset>
    </form>

</div>


<?php
require_once __DIR__ . '/rodape.php';
?>


