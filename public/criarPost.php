<?php
require_once __DIR__ . '/../autoload.php';
if(!estaLogado()){
    alertaJavascript('É necessario estar logado para criar post');
    redireciona('/');
}

if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_FILES['logo'],$_POST['titulo'], $_POST['texto'], $_POST['title'], $_POST['rodape'])){
    $destino = 'imagem/logo_' . rand(1,999) . '.' . pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
    move_uploaded_file($_FILES['logo']['tmp_name'], $destino);
    $prepare = criaConexaoBancoDados()->prepare('insert into post (titulo, texto, logo, title, rodape) value (:titulo, :texto, :logo, :title, :rodape)');
    $prepare->bindParam(':titulo', $_POST['titulo']);
    $prepare->bindParam(':title', $_POST['title']);
    $prepare->bindParam(':texto', $_POST['texto']);
    $prepare->bindParam(':rodape', $_POST['rodape']);
    $prepare->bindParam(':logo', $destino);
    if($prepare->execute()){
        alertaJavascript('Post criado com sucesso');
        redireciona('/');
    }else{
        alertaJavascript('Nao foi possivel criar o post');
        redireciona('/');
    }
}else{
    alertaJavascript('parametros incorreto');
    redireciona('/');
}
