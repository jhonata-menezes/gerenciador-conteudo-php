<?php
require_once __DIR__ . '/../autoload.php';
if($_SERVER['REQUEST_METHOD'] === 'POST'){
    if(isset($_POST['email'], $_POST['senha'], $_POST['nome'])){
        $prepare = criaConexaoBancoDados()->prepare('insert into usuario (nome, email, senha) VALUES (:nome, :email, :senha)');
        $prepare->bindParam(':email', $_POST['email']);
        $prepare->bindParam(':senha', geraHashSenha($_POST['senha']));
        $prepare->bindParam(':nome', $_POST['nome']);
        $result = $prepare->execute();
        if($prepare->rowCount() === 1){
            $_SESSION['logado'] = false;
            alertaJavascript('Cadastro efetuado com sucesso');
            redireciona('/');
        }else{

            alertaJavascript('Nao foi possivel cadastrar');
            redireciona('/');
        }
    }
    else{
        alertaJavascript('Dados do formulario incompletos');
        redireciona('/');
    }
}