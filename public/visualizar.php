<?php
require_once __DIR__ . '/../autoload.php';
if(isset($_GET['id'])){
    $prepare = criaConexaoBancoDados()->prepare('SELECT * FROM post WHERE id=:id');
    $prepare->bindParam(':id', $_GET['id']);
    $prepare->execute();
    if($prepare->rowCount() === 1){
        $registro = $prepare->fetchAll()[0];
    }else{
        alertaJavascript('não foi possivel encontrar o post informado');
        redireciona('/');
    }
}else{
    alertaJavascript('ID não informado');
}
?>

<html xmlns="http://www.w3.org/1999/html">
<head>
    <title><?=$registro['title']?></title>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</head>
<body>

<?php include __DIR__ . '/navbar.php'; ?>


<br/>
<br/>
<br/>

<div class="container">
    <div class="page-header">
        <h1><img src="/<?=$registro['logo']?>" class="img-responsive" alt="Logo" height="200px" width="200px"><?=strtoupper($registro['titulo'])?>
            <a href="/apagar.php?id=<?=$_GET['id']?>" role="button" class="btn btn-danger ">Apagar Post</a></h1>
    </div>
    <div class="row">
        <?=$registro['texto']?>
    </div>

</div>

<footer class="footer">
    <div class="container">
        <p class="text-muted"><?=isset($registro['rodape']) ? $registro['rodape'] : ''?></p>
    </div>
</footer>


