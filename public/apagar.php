<?php
require_once __DIR__ . '/../autoload.php';
if(!estaLogado()){
    alertaJavascript('É necessario estar logado para criar post');
    redireciona('/');
}

if(isset($_GET['id'])){
    $prepare = criaConexaoBancoDados()->prepare('delete from post WHERE id=:id');
    $prepare->bindParam(':id', $_GET['id']);
    $prepare->execute();
    if($prepare->rowCount() === 1){
        alertaJavascript('Post apagado com sucesso');
        redireciona('/');
    }else{
        alertaJavascript('Este id não existe');
        redireciona('/');
    }
}else{
    alertaJavascript('parametro id não informado');
    redireciona('/');
}